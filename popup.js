const API_URL = 'http://192.168.1.130:3000/api/'

new Vue({
  el: '#app',
  data: {
    message: 'Привет, Vue!',
    auth: false,
    groups: [],
    inputAppToken: "",
    plusValue: 0
  },
  created() {
    chrome.storage.local.get(["app_token", "groups"], storage => {
      if(storage.app_token){
        this.auth = true;
        this.groups = storage.groups
        this.app_token = storage.app_token
      }
      else this.auth = false
    });
  },
  watch: {
    inputAppToken: function() {}
  },
  methods: {
    test() {
      chrome.storage.local.get(["app_token", "groups"], storage => {
        console.log(storage)
        alert(this.app_token)
      })
    },
    plus() {
      this.plusValue += 1
    },
    refresh() {
      const METHOD_URL = API_URL + 'groups'
      let headers = new Headers({
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + this.app_token
      });
      let init = { method: 'GET', headers: headers, mode: 'cors', cache: 'default' };
      fetch(METHOD_URL, init)
      .then(function(response) {
        if(response.status == 200) {
          return response.json()
        }
        throw 'Bad request';
      })
      .then(res => {
        chrome.storage.local.set({groups: res}, () => { this.groups = res });
      })
      .catch(error => {
        console.log(error)
      })
    },
    sign_in() {
      const METHOD_URL = API_URL + 'groups'
      let app_token = this.inputAppToken
      let headers = new Headers({
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + app_token
      });
      let init = { method: 'GET', headers: headers, mode: 'cors', cache: 'default' };

      fetch(METHOD_URL, init)
      .then(function(response) {
        if(response.status == 200) {
          return response.json()
        }
        throw 'Bad request';
      })
      .then(res => {
        chrome.storage.local.set({app_token: app_token}, () => { this.app_token = app_token });
        chrome.storage.local.set({groups: res}, () => { this.groups = res });
        this.auth = true
      })
      .catch(error => {
        console.log(error)
      })
    },
    sign_out() {
      chrome.storage.local.clear(() => {
        this.auth = false
        this.groups = []
        this.app_token = ""
      })
    }
  }
})
