let shareList = '<div class="ProfileTweet-action tva_download_action"><button class="ProfileTweet-actionButton u-textUserColorHover js-actionButton" type="button"><div class="IconContainer js-tooltip" data-original-title="Отправить в Sociate">L</div><nav class="groups-list"><ul><li class="soc-send" id="1322">Fresh320</li><li class="soc-send" id="5555">COSMOS404</li></ul></nav></button></div>'

const API_URL = 'https://socializer.com/'

waitForKeyElements ('ytd-grid-video-renderer', insertButton);

function insertButton(target) {
  let video = $(target).closest('ytd-grid-video-renderer');
  if (isVideoButtonExist(video)) return;

  let favIcon = $(video).find('yt-formatted-string')[0];
  $(favIcon).before(shareList);
  $(favIcon).siblings(".tva_download_action").find('li.soc-send').click(videoSender);
}

function isVideoButtonExist(target) {
  return $(target).find('li.soc-send')[0];
}

function videoSender(jNode) {
  let youtube = "https://www.youtube.com"

  let target = $(jNode.target).closest('div#dismissable')

  let video_link = youtube + target.find('#video-title').attr('href');
  let video_title = target.find('#video-title').text();
  let channel_title = target.find('.yt-formatted-string').text();
  let channel_link = youtube + target.find('.yt-formatted-string').attr('href');
  let views = target.find('#metadata-line').find('span').first().text();
  let thumbnail = target.find('.yt-img-shadow').attr('src');
  let id = jNode.target.id;

  data = { id, video_link, video_title, channel_title, channel_link, views, thumbnail }
  console.log(data)

  url = API_URL + 'api/v1/tweet/new'
  jQuery.ajax({
    url: url,
    dataType: 'json',
    data: data,
    success: () => {console.log('Tweet create success')},
    error: () => {console.log('Tweet create error')},
    complete: () => {console.log('Tweet create')}
  })
}

// https://stackoverflow.com/questions/4845215/making-a-chrome-extension-download-a-file/24162238#24162238
// #############################################################################
function waitForKeyElements (
    selectorTxt,    /* Required: The jQuery selector string that
                        specifies the desired element(s).
                    */
    actionFunction, /* Required: The code to run when elements are
                        found. It is passed a jNode to the matched
                        element.
                    */
    bWaitOnce,      /* Optional: If false, will continue to scan for
                        new elements even after the first match is
                        found.
                    */
    iframeSelector  /* Optional: If set, identifies the iframe to
                        search.
                    */
) {
    var targetNodes, btargetsFound;

    if (typeof iframeSelector == "undefined")
        targetNodes     = $(selectorTxt);
    else
        targetNodes     = $(iframeSelector).contents ()
                                           .find (selectorTxt);

    if (targetNodes  &&  targetNodes.length > 0) {
        btargetsFound   = true;
        /*--- Found target node(s).  Go through each and act if they
            are new.
        */
        targetNodes.each ( function () {
            var jThis        = $(this);
            var alreadyFound = jThis.data ('alreadyFound')  ||  false;

            if (!alreadyFound) {
                //--- Call the payload function.
                var cancelFound     = actionFunction (jThis);
                if (cancelFound)
                    btargetsFound   = false;
                else
                    jThis.data ('alreadyFound', true);
            }
        } );
    }
    else {
        btargetsFound   = false;
    }

    //--- Get the timer-control variable for this selector.
    var controlObj      = waitForKeyElements.controlObj  ||  {};
    var controlKey      = selectorTxt.replace (/[^\w]/g, "_");
    var timeControl     = controlObj [controlKey];

    //--- Now set or clear the timer as appropriate.
    if (btargetsFound  &&  bWaitOnce  &&  timeControl) {
        //--- The only condition where we need to clear the timer.
        clearInterval (timeControl);
        delete controlObj [controlKey]
    }
    else {
        //--- Set a timer, if needed.
        if ( ! timeControl) {
            timeControl = setInterval ( function () {
                    waitForKeyElements (    selectorTxt,
                                            actionFunction,
                                            bWaitOnce,
                                            iframeSelector
                                        );
                },
                300
            );
            controlObj [controlKey] = timeControl;
        }
    }
    waitForKeyElements.controlObj   = controlObj;
}
